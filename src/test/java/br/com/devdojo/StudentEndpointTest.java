package br.com.devdojo;

import static br.com.devdojo.config.SecurityConstants.HEADER_STRING;

import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import br.com.devdojo.model.Student;
import br.com.devdojo.repository.StudentRepository;

/**
 * @author cams7
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class StudentEndpointTest {
	@Autowired
	private TestRestTemplate restTemplate;

	@LocalServerPort
	private int port;

	@MockBean
	private StudentRepository repository;

	@Autowired
	private MockMvc mockMvc;

	private HttpEntity<Void> adminHeader;
	private HttpEntity<Void> protectedHeader;
	private HttpEntity<Void> wrongHeader;

	private static final long STUDENT_ID = 10l;

	@Before
	public void configAdminHeader() {
		HttpHeaders headers = restTemplate
				.postForEntity("/login", "{\"username\": \"maria\",\"password\": \"devdojo\"}", String.class)
				.getHeaders();
		adminHeader = new HttpEntity<>(headers);
	}

	@Before
	public void configProtectedHeader() {
		HttpHeaders headers = restTemplate
				.postForEntity("/login", "{\"username\": \"pedro\",\"password\": \"devdojo\"}", String.class)
				.getHeaders();
		protectedHeader = new HttpEntity<>(headers);
	}

	@Before
	public void configWrongHeader() {
		HttpHeaders headers = new HttpHeaders();
		headers.add(HEADER_STRING, "wrong");
		wrongHeader = new HttpEntity<>(headers);
	}

	@Before
	public void setup() {
		Student student = new Student(STUDENT_ID);
		student.setName("Francisco");
		student.setEmail("francisco@test.com");
		student.setBirthDate(LocalDate.of(1985, Month.JULY, 21));

		BDDMockito.when(repository.findById(student.getId())).thenReturn(Optional.of(student));
	}

	@Test
	public void listStudentsWhenTokenIsIncorrectShouldReturnStatusCode403() {
		ResponseEntity<String> response = restTemplate.exchange("/students/protected/list", HttpMethod.GET, wrongHeader,
				String.class);
		Assertions.assertThat(response.getStatusCodeValue()).isEqualTo(HttpStatus.FORBIDDEN.value());
	}

	@Test
	public void getStudentByIdWhenTokenIsIncorrectShouldReturnStatusCode403() {
		ResponseEntity<String> response = restTemplate.exchange("/students/protected/id/{id}", HttpMethod.GET,
				wrongHeader, String.class, 1l);
		Assertions.assertThat(response.getStatusCodeValue()).isEqualTo(HttpStatus.FORBIDDEN.value());
	}

	@Test
	public void listStudentsWhenTokenIsCorrectShouldReturnStatusCode200() {
		Student student1 = new Student(1l);
		student1.setName("Pedro");
		student1.setEmail("pedro@test.com");
		student1.setBirthDate(LocalDate.of(1985, Month.JULY, 21));

		Student student2 = new Student(2l);
		student2.setName("Aline");
		student2.setEmail("aline@test.com");
		student2.setBirthDate(LocalDate.of(1980, Month.SEPTEMBER, 13));

		List<Student> students = Arrays.asList(student1, student2);

		Pageable pageable = PageRequest.of(0, 5);
		Page<Student> page = new PageImpl<Student>(students, pageable, students.size());

		BDDMockito.when(repository.findAll(pageable)).thenReturn(page);

		ResponseEntity<String> response = restTemplate.exchange("/students/protected/list", HttpMethod.GET,
				protectedHeader, String.class);
		Assertions.assertThat(response.getStatusCodeValue()).isEqualTo(HttpStatus.OK.value());
		Assertions.assertThat(response.getBody()).isNotNull();
	}

	@Test
	public void getStudentByIdWhenTokenIsCorrectShouldReturnStatusCode200() {
		Student student = new Student(1l);
		student.setName("Pedro");
		student.setEmail("pedro@test.com");
		student.setBirthDate(LocalDate.of(1985, Month.JULY, 21));

		BDDMockito.when(repository.findById(student.getId())).thenReturn(Optional.of(student));

		ResponseEntity<Student> response = restTemplate.exchange("/students/protected/id/{id}", HttpMethod.GET,
				protectedHeader, Student.class, student.getId());
		Assertions.assertThat(response.getStatusCodeValue()).isEqualTo(HttpStatus.OK.value());
		Assertions.assertThat(response.getBody()).isNotNull();
	}

	@Test
	public void getStudentByIdWhenTokenIsCorrectAndStudentDoesNotExistShouldReturnStatusCode404() {
		ResponseEntity<Student> response = restTemplate.exchange("/students/protected/id/{id}", HttpMethod.GET,
				protectedHeader, Student.class, 1l);
		Assertions.assertThat(response.getStatusCodeValue()).isEqualTo(HttpStatus.NOT_FOUND.value());
	}

	@Test
	public void getStudentsByNameWhenTokenIsCorrectShouldReturnStatusCode200() {
		Student student1 = new Student(1l);
		student1.setName("Mariana");
		student1.setEmail("mariana@test.com");
		student1.setBirthDate(LocalDate.of(1985, Month.JULY, 21));

		Student student2 = new Student(2l);
		student2.setName("Marina");
		student2.setEmail("marina@test.com");
		student2.setBirthDate(LocalDate.of(1980, Month.SEPTEMBER, 13));

		Student student3 = new Student(3l);
		student3.setName("Maris");
		student3.setEmail("maria@test.com");
		student3.setBirthDate(LocalDate.of(1988, Month.FEBRUARY, 25));

		List<Student> students = Arrays.asList(student1, student2, student3);

		BDDMockito.when(repository.findByNameIgnoreCaseContaining("mari")).thenReturn(students);

		ResponseEntity<Student[]> response = restTemplate.exchange("/students/protected/name/{name}", HttpMethod.GET,
				protectedHeader, Student[].class, "mari");
		Assertions.assertThat(response.getStatusCodeValue()).isEqualTo(HttpStatus.OK.value());
		Assertions.assertThat(response.getBody().length).isEqualTo(3);
	}

	@Test
	public void deleteWhenUserHasRoleAdminAndStudentExistsShouldResturnStatusCode200() {
		BDDMockito.doNothing().when(repository).deleteById(STUDENT_ID);
		ResponseEntity<String> response = restTemplate.exchange("/students/admin/id/{id}", HttpMethod.DELETE,
				adminHeader, String.class, STUDENT_ID);
		Assertions.assertThat(response.getStatusCodeValue()).isEqualTo(HttpStatus.OK.value());
	}

	@Test
	public void deleteWhenUserHasRoleAdminAndStudentDoesNotExistShouldResturnStatusCode404() throws Exception {
		String token = adminHeader.getHeaders().get(HEADER_STRING).get(0);
		BDDMockito.doNothing().when(repository).deleteById(STUDENT_ID);
		mockMvc.perform(MockMvcRequestBuilders.delete("/students/admin/id/{id}", -1l).header(HEADER_STRING, token))
				.andExpect(MockMvcResultMatchers.status().isNotFound());
	}

	@Test
	public void deleteWhenUserDoesNotHaveRoleAdminShouldResturnStatusCode403() throws Exception {
		String token = protectedHeader.getHeaders().get(HEADER_STRING).get(0);
		BDDMockito.doNothing().when(repository).deleteById(STUDENT_ID);
		mockMvc.perform(
				MockMvcRequestBuilders.delete("/students/admin/id/{id}", STUDENT_ID).header(HEADER_STRING, token))
				.andExpect(MockMvcResultMatchers.status().isForbidden());
	}

	@Test
	public void createWhenNameIsNullShouldReturnStatusCode400() {
		Student student = new Student(1l);
		student.setEmail("carlos@test.com");
		student.setBirthDate(LocalDate.of(1985, Month.JULY, 21));

		BDDMockito.when(repository.save(student)).thenReturn(student);
		ResponseEntity<String> response = restTemplate.exchange("/students/admin", HttpMethod.POST,
				new HttpEntity<>(student, adminHeader.getHeaders()), String.class);
		Assertions.assertThat(response.getStatusCodeValue()).isEqualTo(HttpStatus.BAD_REQUEST.value());
		Assertions.assertThat(response.getBody()).contains("message", "não pode estar vazio");
	}

	@Test
	public void createShouldPersistDataAndReturnStatusCode200() {
		Student student = new Student(1l);
		student.setName("Carlos");
		student.setEmail("carlos@test.com");
		student.setBirthDate(LocalDate.of(1985, Month.JULY, 21));

		BDDMockito.when(repository.save(student)).thenReturn(student);
		ResponseEntity<Student> response = restTemplate.exchange("/students/admin", HttpMethod.POST,
				new HttpEntity<>(student, adminHeader.getHeaders()), Student.class);
		Assertions.assertThat(response.getStatusCodeValue()).isEqualTo(HttpStatus.OK.value());
		Assertions.assertThat(response.getBody().getId()).isNotNull();
	}
}

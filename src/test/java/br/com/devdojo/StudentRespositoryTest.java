/**
 * 
 */
package br.com.devdojo;

import java.time.LocalDate;
import java.time.Month;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import javax.validation.ConstraintViolationException;

import org.assertj.core.api.Assertions;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.devdojo.model.Student;
import br.com.devdojo.repository.StudentRepository;

/**
 * @author cams7
 *
 */
@RunWith(SpringRunner.class)
@DataJpaTest
//@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class StudentRespositoryTest {

	private static final String NAME = "Maria";
	private static final String EMAIL = "maria@test.com";
	private static final LocalDate BIRTH_DATE = LocalDate.of(1975, Month.MAY, 17);

	@Autowired
	private StudentRepository repository;

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Test
	public void createShouldPersistData() {
		Student student = saveNewStudent();

		Assertions.assertThat(student.getId()).isNotNull();
		verifyStudentData(student, NAME, EMAIL, BIRTH_DATE);

		verifyThereIsOnlyOneInstance();
	}

	@Test
	public void deleteShouldRemoveData() {
		Student student = saveNewStudent();

		repository.deleteById(student.getId());

		Assertions.assertThatThrownBy(() -> {
			repository.findById(student.getId()).get();
		}).isInstanceOf(NoSuchElementException.class).hasMessageContaining("No value present");

		Iterator<?> iterator = repository.findAll().iterator();
		Assertions.assertThat(iterator.hasNext()).isFalse();
	}

	@Test
	public void updateShouldChangeAndPersistData() {
		Student student = saveNewStudent();

		final String NAME_CHANGED = "Pedro";
		final String EMAIL_CHANGED = "pedro@test.com";
		final LocalDate BIRTH_DATE_CHANGED = LocalDate.of(1985, Month.JULY, 21);

		student.setName(NAME_CHANGED);
		student.setEmail(EMAIL_CHANGED);
		student.setBirthDate(BIRTH_DATE_CHANGED);

		repository.save(student);
		student = repository.findById(student.getId()).get();

		verifyStudentData(student, NAME_CHANGED, EMAIL_CHANGED, BIRTH_DATE_CHANGED);

		verifyThereIsOnlyOneInstance();
	}

	@Test
	public void findByNameShouldIgnoreCaseContaining() {
		saveNewStudent();

		final String NAME_CHANGED = "Mariana";
		final String EMAIL_CHANGED = "mariana@test.com";
		final LocalDate BIRTH_DATE_CHANGED = LocalDate.of(1989, Month.AUGUST, 25);

		Student student = new Student();
		student.setName(NAME_CHANGED);
		student.setEmail(EMAIL_CHANGED);
		student.setBirthDate(BIRTH_DATE_CHANGED);

		repository.save(student);

		List<Student> students = repository.findByNameIgnoreCaseContaining("ma");
		Assertions.assertThat(students.size()).isEqualTo(2);
	}

	@Test
	public void createWhenNameIsNullThrowConstraintViolationException() {
		thrown.expect(ConstraintViolationException.class);
		thrown.expectMessage("não pode estar vazio");
		Student student = new Student();
		student.setBirthDate(LocalDate.of(1981, Month.JUNE, 2));
		repository.save(student);
	}

	@Test
	public void createWhenBirthDateIsNullThrowConstraintViolationException() {
		thrown.expect(ConstraintViolationException.class);
		thrown.expectMessage("não pode ser nulo");
		Student student = new Student();
		student.setName("Aline");
		repository.save(student);
	}

	@Test
	public void createWhenEmailIsNotValidThrowConstraintViolationException() {
		thrown.expect(ConstraintViolationException.class);
		thrown.expectMessage("não é um endereço de e-mail");
		Student student = new Student();
		student.setName("Aline");
		student.setEmail("test");
		student.setBirthDate(LocalDate.of(1981, Month.JUNE, 2));
		repository.save(student);
	}

	private Student saveNewStudent() {
		Student student = new Student();
		student.setName(NAME);
		student.setEmail(EMAIL);
		student.setBirthDate(BIRTH_DATE);

		return repository.save(student);
	}

	private void verifyStudentData(Student student, String name, String email, LocalDate birthDate) {
		Assertions.assertThat(student.getName()).isEqualTo(name);
		Assertions.assertThat(student.getEmail()).isEqualTo(email);
		Assertions.assertThat(student.getBirthDate()).isEqualTo(birthDate);
	}

	private void verifyThereIsOnlyOneInstance() {
		Iterator<?> iterator = repository.findAll().iterator();
		Assertions.assertThat(iterator.hasNext()).isTrue();
		Assertions.assertThat(iterator.next()).isNotNull();
		Assertions.assertThat(iterator.hasNext()).isFalse();
	}

}

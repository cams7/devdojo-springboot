/**
 * 
 */
package br.com.devdojo.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import br.com.devdojo.model.Student;

/**
 * @author cams7
 *
 */
@Repository
public interface StudentRepository extends PagingAndSortingRepository<Student, Long> {
	List<Student> findByNameIgnoreCaseContaining(String name);
}

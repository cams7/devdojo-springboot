/**
 * 
 */
package br.com.devdojo.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.devdojo.model.User;

/**
 * @author cams7
 *
 */
@Repository
public interface UserRepository extends CrudRepository<User, Long> {
	User findByUsername(String username);
}

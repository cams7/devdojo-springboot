/**
 * 
 */
package br.com.devdojo.handler;

import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.DefaultResponseErrorHandler;

/**
 * @author cams7
 *
 */
public class RestResponseExceptionHandler extends DefaultResponseErrorHandler {

	@Override
	public boolean hasError(ClientHttpResponse response) throws IOException {
		if (response.getStatusCode() != HttpStatus.OK)
			System.err.println("Inside hasError");
		return super.hasError(response);
	}

	@Override
	public void handleError(ClientHttpResponse response) throws IOException {
		System.err.println("Doing something with status code " + response.getStatusCode());
		System.err.println("Doing something with status body " + IOUtils.toString(response.getBody(), "UTF-8"));
		super.handleError(response);
	}
}

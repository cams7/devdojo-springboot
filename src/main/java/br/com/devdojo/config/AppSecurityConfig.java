/**
 * 
 */
package br.com.devdojo.config;

import static br.com.devdojo.config.SecurityConstants.SIGN_UP_URL;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;

import br.com.devdojo.service.CustomUserDetailsService;

/**
 * @author cams7
 *
 */
@EnableWebSecurity
public class AppSecurityConfig extends WebSecurityConfigurerAdapter {

	private static final String PROTECTED_PREFIX = "protected";
	private static final String ADMIN_PREFIX = "admin";

	@Autowired
	private CustomUserDetailsService service;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.cors().configurationSource(request -> new CorsConfiguration().applyPermitDefaultValues()).and().csrf()
				.disable().authorizeRequests().antMatchers(HttpMethod.GET, SIGN_UP_URL).permitAll()
				.antMatchers(String.format("/*/%s/**", PROTECTED_PREFIX)).hasRole("USER")
				.antMatchers(String.format("/*/%s/**", ADMIN_PREFIX)).hasRole("ADMIN").and()
				.addFilter(new JWTAuthenticationFilter(authenticationManager()))
				.addFilter(new JWTAuthorizationFilter(authenticationManager(), service));
	}

	@Autowired
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(service).passwordEncoder(new BCryptPasswordEncoder());
	}
}

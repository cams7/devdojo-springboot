/**
 * 
 */
package br.com.devdojo.config;

/**
 * @author cams7
 *
 */
public class SecurityConstants {
	// Authorization Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ
	public static final String SECRET = "DevDojoSpringBoot";
	public static final String TOKEN_PREFIX = "Bearer ";
	public static final String HEADER_STRING = "Authorization";
	public static final String SIGN_UP_URL = "/users/sign-up";
	public static final long EXPIRATION_TIME = 86400000l; // 1000x60x60x24 = 1 day
}

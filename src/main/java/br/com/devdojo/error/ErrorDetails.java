/**
 * 
 */
package br.com.devdojo.error;

/**
 * @author cams7
 *
 */
public class ErrorDetails {
	private String title;
	private int status;
	private String detail;
	private long timestamp;
	private String developerMessage;

	protected ErrorDetails() {
		super();
	}

	public String getTitle() {
		return title;
	}

	protected void setTitle(String title) {
		this.title = title;
	}

	public int getStatus() {
		return status;
	}

	protected void setStatus(int status) {
		this.status = status;
	}

	public String getDetail() {
		return detail;
	}

	protected void setDetail(String detail) {
		this.detail = detail;
	}

	public long getTimestamp() {
		return timestamp;
	}

	protected void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public String getDeveloperMessage() {
		return developerMessage;
	}

	protected void setDeveloperMessage(String developerMessage) {
		this.developerMessage = developerMessage;
	}

	public static final class Builder extends ErrorDetails {

		private Builder() {
			super();
		}

		public static Builder newBuilder() {
			return new Builder();
		}

		public Builder title(String title) {
			setTitle(title);
			return this;
		}

		public Builder status(int status) {
			setStatus(status);
			return this;
		}

		public Builder detail(String detail) {
			setDetail(detail);
			return this;
		}

		public Builder timestamp(long timestamp) {
			setTimestamp(timestamp);
			return this;
		}

		public Builder developerMessage(String developerMessage) {
			setDeveloperMessage(developerMessage);
			return this;
		}

		public ErrorDetails build() {
			ErrorDetails details = new ErrorDetails();
			details.setTitle(getTitle());
			details.setStatus(getStatus());
			details.setDetail(getDetail());
			details.setTimestamp(getTimestamp());
			details.setDeveloperMessage(getDeveloperMessage());
			return details;
		}

	}

}

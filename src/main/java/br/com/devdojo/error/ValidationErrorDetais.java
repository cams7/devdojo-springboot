/**
 * 
 */
package br.com.devdojo.error;

/**
 * @author cams7
 *
 */
public class ValidationErrorDetais extends ErrorDetails {

	private Field[] fields;

	private ValidationErrorDetais() {
		super();
	}

	public Field[] getFields() {
		return fields;
	}

	protected void setFields(Field[] fields) {
		this.fields = fields;
	}

	public static final class ValidationBuilder extends ValidationErrorDetais {
		
		private ValidationBuilder() {
			super();
		}

		public static ValidationBuilder newBuilder() {
			return new ValidationBuilder();
		}

		public ValidationBuilder title(String title) {
			setTitle(title);
			return this;
		}

		public ValidationBuilder status(int status) {
			setStatus(status);
			return this;
		}

		public ValidationBuilder detail(String detail) {
			setDetail(detail);
			return this;
		}

		public ValidationBuilder timestamp(long timestamp) {
			setTimestamp(timestamp);
			return this;
		}

		public ValidationBuilder developerMessage(String developerMessage) {
			setDeveloperMessage(developerMessage);
			return this;
		}

		public ValidationBuilder fields(Field... fields) {
			this.setFields(fields);
			return this;
		}

		public ValidationErrorDetais build() {
			ValidationErrorDetais details = new ValidationErrorDetais();
			details.setTitle(getTitle());
			details.setStatus(getStatus());
			details.setDetail(getDetail());
			details.setTimestamp(getTimestamp());
			details.setDeveloperMessage(getDeveloperMessage());
			details.setFields(getFields());
			return details;
		}

	}

	public static final class Field {
		private String name;
		private String message;

		public Field(String name, String message) {
			super();
			this.name = name;
			this.message = message;
		}

		public String getName() {
			return name;
		}

		public String getMessage() {
			return message;
		}

	}

}

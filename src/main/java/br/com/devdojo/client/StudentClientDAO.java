/**
 * 
 */
package br.com.devdojo.client;

import java.util.Arrays;
import java.util.List;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import br.com.devdojo.handler.RestResponseExceptionHandler;
import br.com.devdojo.model.PageableResponse;
import br.com.devdojo.model.Student;

/**
 * @author cams7
 *
 */
public class StudentClientDAO {
	private final static RestTemplate REST_TEMPLATE_PROTECTED = new RestTemplateBuilder()
			.rootUri("http://localhost:8080/students/protected").basicAuthentication("pedro", "devdojo")
			.errorHandler(new RestResponseExceptionHandler()).build();

	private final static RestTemplate REST_TEMPLATE_ADMIN = new RestTemplateBuilder()
			.rootUri("http://localhost:8080/students/admin").basicAuthentication("maria", "devdojo")
			.errorHandler(new RestResponseExceptionHandler()).build();

	public Student findById(long id) {
//		Student student = restTemplateProtected.getForObject("/id/{id}", Student.class, id);
//		return student;

		ResponseEntity<Student> entity = REST_TEMPLATE_PROTECTED.getForEntity("/id/{id}", Student.class, id);
		return entity.getBody();
	}

	public List<Student> findByName(String name) {
		Student[] students = REST_TEMPLATE_PROTECTED.getForObject("/name/{name}", Student[].class, name);
		return Arrays.asList(students);
	}

	public List<Student> listAll(int page, int size, String sortedField) {
//		Student[] students = restTemplate.getForObject("/list", Student[].class);
//		System.out.println(Arrays.toString(students));

		ResponseEntity<PageableResponse<Student>> entities = REST_TEMPLATE_PROTECTED.exchange(
				String.format("/list?page=%d&size=%d&sort=%s,asc", page, size, sortedField), HttpMethod.GET, null,
				new ParameterizedTypeReference<PageableResponse<Student>>() {
				});
		return entities.getBody().getContent();
	}

	public Student save(Student student) {
//		student = REST_TEMPLATE_ADMIN.postForObject("/", student, Student.class);
//		return student;

//		ResponseEntity<Student> entity = REST_TEMPLATE_ADMIN.postForEntity("/", student, Student.class);
//		return entity.getBody();

		ResponseEntity<Student> entity = REST_TEMPLATE_ADMIN.exchange("/", HttpMethod.POST,
				new HttpEntity<>(student, createJSONHeader()), Student.class);
		return entity.getBody();
	}

	public void update(Student student) {
		REST_TEMPLATE_ADMIN.put("/", student, Student.class);
	}

	public void delete(long id) {
		REST_TEMPLATE_ADMIN.delete("/id/{id}", id);
	}

	private static HttpHeaders createJSONHeader() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
		return headers;
	}
}

/**
 * 
 */
package br.com.devdojo.client;

import java.time.Instant;
import java.time.LocalDate;
import java.util.List;
import java.util.Random;

import br.com.devdojo.model.Student;

/**
 * @author cams7
 *
 */
public class SpringClientTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		StudentClientDAO dao = new StudentClientDAO();

		System.out.println(dao.findById(1l));

		System.out.println(dao.listAll(2, 3, "name"));

		System.out.println(dao.save(getRamdonStudent("Alice")));

		List<Student> students = dao.findByName("alic");
		if (!students.isEmpty()) {
			dao.update(getRamdonStudent(students.get(0), "Biana"));
			if (students.size() > 1)
				dao.delete(students.get(students.size() - 1).getId());
		}

		Student student = new Student();
		student.setName("Maria");
		student.setEmail("maria1@test.com");
		student.setBirthDate(LocalDate.of(getRandomNumber(1975, 2010), getRandomNumber(1, 12), getRandomNumber(1, 28)));

		System.out.println(dao.save(student));

	}

	private static int getRandomNumber(int minimum, int maximum) {
		Random rn = new Random();
		int range = maximum - minimum + 1;
		int randomNumber = rn.nextInt(range) + minimum;
		return randomNumber;
	}

	private static Student getRamdonStudent(Student student, String name) {
		long millis = Instant.now().toEpochMilli();

		student.setName(name + millis);
		student.setEmail(name.toLowerCase() + millis + "@test.com");
		student.setBirthDate(LocalDate.of(getRandomNumber(1975, 2010), getRandomNumber(1, 12), getRandomNumber(1, 28)));
		return student;
	}

	private static Student getRamdonStudent(String name) {
		return getRamdonStudent(new Student(), name);
	}

}

/**
 * 
 */
package br.com.devdojo.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.tomcat.util.codec.binary.Base64;
import org.apache.tomcat.util.http.fileupload.IOUtils;

/**
 * @author cams7
 *
 */
public class JavaClientTest {

	private static final String USERNAME = "pedro";
	private static final String PASSWORD = "devdojo";

	public static void main(String[] args) {
		HttpURLConnection connection = null;
		BufferedReader reader = null;

		try {
			URL url = new URL("http://localhost:8080/students/protected/id/1");
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.addRequestProperty("Authorization", "Basic " + encodeUsernamePassword(USERNAME, PASSWORD));
			reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			StringBuilder builder = new StringBuilder();
			String line;
			while ((line = reader.readLine()) != null) {
				builder.append(line);
			}
			System.out.println(builder);
		} catch (IOException e) {
			System.err.println(e.getMessage());
		} finally {
			IOUtils.closeQuietly(reader);
			if (connection != null)
				connection.disconnect();
		}
	}

	private static String encodeUsernamePassword(String username, String password) {
		String usernameAndPassword = username + ":" + password;
		return new String(Base64.encodeBase64(usernameAndPassword.getBytes()));
	}

}

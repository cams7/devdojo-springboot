/**
 * 
 */
package br.com.devdojo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

//import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author cams7
 *
 */
@SuppressWarnings("serial")
@Entity
public class User extends AbstractEntity {
	@NotEmpty
	@Size(min = 3, max = 20)
	private String username;

	@NotEmpty
//	@JsonIgnore
	private String password;

	@NotEmpty
	@Size(min = 3, max = 30)
	private String name;

	@Column(nullable = false)
	private boolean admin;

	public User() {
		super();
	}

	public User(Long id) {
		super(id);
	}

	public User(String username) {
		this();
		this.username = username;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format("%s [id=%d, username=%s, name=%s, admin=%s]", getClass().getSimpleName(), getId(),
				username, name, admin);
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

}

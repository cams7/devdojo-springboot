/**
 * 
 */
package br.com.devdojo.model;

import java.util.List;

import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;

/**
 * @author cams7
 *
 */
@SuppressWarnings("serial")
public class PageableResponse<T> extends PageImpl<T> {

	private boolean last;
	private boolean first;
	private int totalPages;
	private int numberOfElements;

	@JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
	public PageableResponse(@JsonProperty("content") List<T> content, @JsonProperty("number") int page,
			@JsonProperty("size") int size, @JsonProperty("totalElements") long totalElements,
			@JsonProperty("pageable") JsonNode pageable, @JsonProperty("totalPages") int totalPages,
			@JsonProperty("last") boolean last, @JsonProperty("first") boolean first,
			@JsonProperty("sort") JsonNode sort, @JsonProperty("numberOfElements") int numberOfElements) {

		super(content, PageRequest.of(page, size), totalElements);

		this.totalPages = totalPages;
		this.last = last;
		this.first = first;
		this.numberOfElements = numberOfElements;
	}

	/**
	 * @return the last
	 */
	public boolean isLast() {
		return last;
	}

	/**
	 * @return the first
	 */
	public boolean isFirst() {
		return first;
	}

	/**
	 * @return the totalPages
	 */
	public int getTotalPages() {
		return totalPages;
	}

	/**
	 * @return the numberOfElements
	 */
	public int getNumberOfElements() {
		return numberOfElements;
	}

}

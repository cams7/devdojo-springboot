/**
 * 
 */
package br.com.devdojo.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.persistence.Entity;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author cams7
 *
 */
@Entity
@SuppressWarnings("serial")
public class Student extends AbstractEntity {
	@NotEmpty
	@Size(min = 3, max = 30)
	private String name;

	@Email
	@Size(max = 50)
	private String email;

	@NotNull
	private LocalDate birthDate;

	public Student() {
		super();
	}

	public Student(Long id) {
		super(id);
	}

	public Student(Long id, String name) {
		this(id);
		this.name = name;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format("%s [id=%d, name=%s, email=%s, birthDate=%s]", this.getClass().getSimpleName(), getId(),
				name, email, DateTimeFormatter.ofPattern("dd/MM/yyyy").format(birthDate));
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

}

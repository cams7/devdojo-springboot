/**
 * 
 */
package br.com.devdojo.endpoint;

import java.time.LocalDate;
import java.time.Month;
import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.devdojo.error.InvalidDataException;
import br.com.devdojo.error.ResourceNotFoundException;
import br.com.devdojo.model.Student;
import br.com.devdojo.repository.StudentRepository;
import io.swagger.annotations.ApiOperation;

/**
 * @author cams7
 *
 */
@RestController
@RequestMapping("students")
public class StudentEndpoint {

	private static final String PROTECTED_PREFIX = "protected";
	private static final String ADMIN_PREFIX = "admin";

	@Autowired
	private StudentRepository repository;

	/**
	 * @param pageable
	 * 
	 * @example URL
	 *          http://localhost:8080/students/protected/list?page=1&size=5&sort=birthDate,desc&sort=name
	 * @return
	 */
	@GetMapping(path = PROTECTED_PREFIX + "/list")
	@ApiOperation(value = "Return a list with all students", response = Student[].class)
	public ResponseEntity<?> listAll(Pageable pageable, Authentication authentication) {
		System.out.println("Logged user: " + authentication.getPrincipal());

		Iterable<Student> students = repository.findAll(pageable);

		return new ResponseEntity<>(students, HttpStatus.OK);
	}

	@GetMapping(path = PROTECTED_PREFIX + "/id/{id}")
	@ApiOperation(value = "Return a student by id", response = Student.class)
	public ResponseEntity<?> getStudentById(@PathVariable Long id, Authentication authentication) {
		System.out.println("Logged user: " + authentication.getPrincipal());

		Student student = repository.findById(id).get();
		return new ResponseEntity<>(student, HttpStatus.OK);
	}

	@GetMapping(path = PROTECTED_PREFIX + "/name/{name}")
	@ApiOperation(value = "Returns a list of students who have name with informed characters", response = Student[].class)
	public ResponseEntity<?> getStudentsByName(@PathVariable String name, Authentication authentication) {
		System.out.println("Logged user: " + authentication.getPrincipal());

		List<Student> students = repository.findByNameIgnoreCaseContaining(name);

		verifyIfStudentsExist(students, name);

		return new ResponseEntity<>(students, HttpStatus.OK);
	}

	@PostMapping(path = ADMIN_PREFIX)
	@Transactional
	@ApiOperation(value = "Save a student data", response = Student.class)
	public ResponseEntity<?> save(@Valid @RequestBody Student student, Authentication authentication) {
		System.out.println("Logged user: " + authentication.getPrincipal());

		student = repository.save(student);
		String studentName = student.getName();

		if ("maria".equalsIgnoreCase(student.getName())) {
			saveAnotherStudent("César");
		}
		verifyInvalidName(studentName, "maria");

		return new ResponseEntity<>(student, HttpStatus.OK);
	}

	@DeleteMapping(path = ADMIN_PREFIX + "/id/{id}")
	@Transactional
	@ApiOperation(value = "Remove a student by informed id", response = Void.class)
	public ResponseEntity<?> delete(@PathVariable Long id, Authentication authentication) {
		System.out.println("Logged user: " + authentication.getPrincipal());

		Student studentFound = repository.findById(id).get();
		String studentName = studentFound.getName();
		repository.deleteById(id);

		if ("maria".equalsIgnoreCase(studentFound.getName())) {
			saveAnotherStudent("César");
		}

		verifyInvalidName(studentName, "maria");

		return new ResponseEntity<>(HttpStatus.OK);
	}

	@PutMapping(path = ADMIN_PREFIX)
	@Transactional
	@ApiOperation(value = "Update a student data", response = Void.class)
	public ResponseEntity<?> update(@Valid @RequestBody Student student, Authentication authentication) {
		System.out.println("Logged user: " + authentication.getPrincipal());

		Student studentFound = repository.findById(student.getId()).get();
		String studentName = studentFound.getName();
		repository.save(student);

		if ("maria".equalsIgnoreCase(studentFound.getName())) {
			saveAnotherStudent("César");
		}

		verifyInvalidName(studentName, "maria");

		return new ResponseEntity<>(HttpStatus.OK);
	}

	private void verifyIfStudentsExist(Collection<Student> students, String name) {
		if (students.isEmpty())
			throw new ResourceNotFoundException("Student not found for name: " + name);
	}

	private void verifyInvalidName(String name1, String name2) {
		if (name2.equalsIgnoreCase(name1))
			throw new InvalidDataException(name1 + " already exists or can't be changed");
	}

	private Student saveAnotherStudent(String name) {
		Student student = new Student();
		student.setName(name);
		student.setBirthDate(LocalDate.of(1983, Month.MARCH, 5));
		student.setEmail("ceanma@gmail.com");
		return repository.save(student);
	}
}

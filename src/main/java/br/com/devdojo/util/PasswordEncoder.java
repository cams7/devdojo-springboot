/**
 * 
 */
package br.com.devdojo.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @author cams7
 *
 */
public class PasswordEncoder {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		System.out.println(encoder.encode("devdojo"));
	}

}

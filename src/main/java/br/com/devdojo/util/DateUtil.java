/**
 * 
 */
package br.com.devdojo.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author cams7
 *
 */
public final class DateUtil {
	public static String formatLocalDateTimeToDatabaseStyle(LocalDateTime localDateTime) {
		return DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss").format(localDateTime);
	}
}

--DDL - Data Definition Language - Linguagem de Definição de Dados.
--São os comandos que interagem com os objetos do banco.
--São comandos DDL : CREATE, ALTER e DROP

--DML - Data Manipulation Language - Linguagem de Manipulação de Dados.
--São os comandos que interagem com os dados dentro das tabelas.
--São comandos DML : INSERT, DELETE e UPDATE

--DQL - Data Query Language - Linguagem de Consulta de dados.
--São os comandos de consulta.
--São comandos DQL : SELECT (é o comando de consulta)
--Aqui cabe um parenteses. Em alguns livros o SELECT fica na DML em outros tem esse grupo próprio.

--DTL - Data Transaction Language - LInguage de Transação de Dados.
--São os comandos para controle de transação.
--São comandos DTL : BEGIN TRANSACTION, COMMIT E ROLLBACK

--DCL - Data Control Language - Linguagem de Controle de Dados.
--São os comandos para controlar a parte de segurança do banco de dados.
--São comandos DCL : GRANT, REVOKE E DENY.

insert student(name, email, birth_date) values('Maria','maria@test.com', '2001-07-23');
insert student(name, email, birth_date) values('João','joao@test.com', '2005-08-11');
insert student(name, email, birth_date) values('Pedro','pedro@test.com', '1999-03-15');
insert student(name, email, birth_date) values('Francisco','francisco@test.com', '1989-05-13');
insert student(name, email, birth_date) values('Franciele','franciele@test.com', '1987-07-23');
insert student(name, email, birth_date) values('Mariana','mariana@test.com', '1989-01-25');
insert student(name, email, birth_date) values('Luciano','luciano@test.com', '1986-04-22');
insert student(name, email, birth_date) values('Ana','ana@test.com', '2004-04-22');
insert student(name, email, birth_date) values('Josiana','josiana@test.com', '2005-05-22');
insert student(name, email, birth_date) values('Carlos',null, '2006-09-20');
insert student(name, email, birth_date) values('Carolina',null, '2005-12-02');
insert student(name, email, birth_date) values('Carol','carol@test.com', '2000-10-03'); 

SELECT id,birth_date,email,name FROM student;

update student set name='Maria' where id=1;

delete from user;
--password: devdojo
insert user(admin,name,password,username) values(0, 'Pedro Silva', '$2a$10$IcLXSat9jFY.1BmP5HHJmecXeIKpZgz7hKByiDkjeJXtKySO/zKKy', 'pedro');
insert user(admin,name,password,username) values(1, 'Maria Almeida', '$2a$10$IcLXSat9jFY.1BmP5HHJmecXeIKpZgz7hKByiDkjeJXtKySO/zKKy', 'maria');
SELECT id,admin,name,password,username FROM user;


